export const state = () => ({
  // pages: [],
  // menu: [],
  // accueil: [],
  // footer: [],
  isLoading: true,
  micronutrition: [],
})



export const mutations = {
  // updatePages: (state, pages) => {
  //   state.pages = pages
  // },
  // updateMenu: (state, menu) => {
  //   state.menu = menu
  // },
  // updateAccueil: (state, accueil) => {
  //   state.accueil = accueil
  // },
  // updateFooter: (state, footer) => {
  //   state.footer = footer
  // },
  updatePreloader: (state, isLoading) => {
    state.isLoading = isLoading
  },
  updateMicronutrition: (state, micronutrition) => {
    state.micronutrition = micronutrition
  },
}

export const actions = {
  // async getPages({ state, commit }) {
  //   if (state.pages.length) return
  //   try {
  //     let pages = await fetch(`http://localhost:8888/wp-micronutrition/wp-json/wp/v2/pages`
  //     ).then(res => res.json())
  //     pages = pages
  //       .filter(el => el.status === "publish")
  //       .map(({ id, slug, title, excerpt, date, tags, content, acf}) => ({
  //         id,
  //         slug,
  //         title,
  //         excerpt,
  //         date,
  //         tags,
  //         content,
  //         acf
  //       }))
  //       // console.log(pages, "pages");
  //       commit("updatePages", pages)
  //   } catch (err) {
  //     console.log(err);    
  //   }
  // },
  // async getMenu({ state, commit }) {
  //   if (state.menu.length) return
  //   try {
  //     let menu = await fetch(`${process.env.API_DF}wp-json/menus/v1/menus/appnav`
  //     ).then(res => res.json())
  //     menu = menu.items
  //     // console.log(menu, "menu");
  //     commit("updateMenu", menu)
  //   } catch (err) {
  //     console.log(err);    
  //   }
  // },

  // async getAccueil({ state, commit }) {
  //   if (state.accueil.length) return
  //   try {
  //     let accueil = await fetch(`${process.env.API_DF}wp-json/acf/v3/pages`
  //     ).then(res => res.json())
  //     accueil = accueil
  //     .filter(el => el.id === 9)
  //     .map(({ id, title, acf}) => ({
  //       id,
  //       title,
  //       acf
  //     }))
  //       // console.log(accueil, "accueil");
  //       commit("updateAccueil", accueil);
  //   } catch (err) {
  //     console.log(err);    
  //   }
  // },


  // async getFooter({ state, commit }) {
  //   if (state.footer.length) return
  //   try {
  //     let footer = await fetch(`${process.env.API_DF}wp-json/acf/v3/pages`
  //     ).then(res => res.json())
  //     footer = footer
  //     .filter(el => el.id === 126)
  //     .map(({ id, acf}) => ({
  //       id,
  //       acf
  //     }))
  //       // console.log(footer, "footer");
  //       commit("updateFooter", footer);
  //   } catch (err) {
  //     console.log(err);    
  //   }
  // },

  async getMicronutrition({ state, commit }) {
    if (state.micronutrition.length) return
    try {
      let micronutrition = await fetch(`${process.env.API_DF}wp-json/wp/v2/micronutrition`
      ).then(res => res.json())
      micronutrition = Object.values(micronutrition)
      .map(({ id, acf}) => ({
        id,
        acf
      }))
        // console.log(micronutrition, "micro");
        commit("updateMicronutrition", micronutrition)
    } catch (err) {
      console.log(err);    
    }
  },
}

